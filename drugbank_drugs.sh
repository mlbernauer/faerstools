#!/bin/bash

# Usage: ./drugbank_drugs.sh <password> <outfile>

mysql -uroot -p$1 drugbank -e "select DrugBank.Name, DrugBankSynonyms.Synonym from DrugBank inner join DrugBankSynonyms on DrugBank.WID = DrugBankSynonyms.DrugBank_WID " > $2

mysql -uroot -p$1 drugbank -e "select DrugBank.Name, DrugBankBrands.Brand from DrugBank inner join DrugBankBrands on DrugBankBrands.DrugBank_WID = DrugBank.WID" > $2

mv $2 tmp.txt
cat tmp.txt | sort | uniq > $2
