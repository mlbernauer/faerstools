PT	PRR	high	low
"mantle cell lymphoma refractory"	4.717224	12.249201	1.816625
"urine human chorionic gonadotropin positive"	4.548752	11.780278	1.756422
"leukaemia plasmacytic"	1.896049	2.343209	1.534222
"multiple myeloma"	1.528072	1.581530	1.476421
"blood human chorionic gonadotropin abnormal"	2.959520	4.376826	2.001166
"blood human chorionic gonadotropin"	10.613754	30.127169	3.739208
"spleen operation"	1.922491	5.311727	0.695814
"poems syndrome"	5.306877	13.908744	2.024837
"superficial spreading melanoma stage ii"	4.130758	9.786990	1.743454
"myelodysplastic syndrome transformation"	3.106029	3.635130	2.653940
"factor xii deficiency"	5.993649	17.812441	2.016783
"blood human chorionic gonadotropin increased"	4.541525	5.274412	3.910473
"full blood count decreased"	2.133969	2.354846	1.933809
"ocular retrobulbar haemorrhage"	1.083958	2.948331	0.398518
"leukaemic infiltration extramedullary"	1.171173	3.190787	0.429877
"malignant melanoma stage ii"	1.157864	2.837151	0.472534
"b-cell type acute leukaemia"	2.426001	6.765502	0.869925
"acute leukaemia"	1.189265	1.412278	1.001468
"pregnancy test urine positive"	1.638615	3.014707	0.890653
"erythroleukaemia"	1.088590	2.664298	0.444781
"richter's syndrome"	2.701683	5.888277	1.239597
"human chorionic gonadotropin abnormal"	91.702831	246.995145	34.046860
"tumour flare"	2.511781	3.087611	2.043341
"5q-syndrome"	6.792802	20.466818	2.254486
"human chorionic gonadotropin increased"	8.817580	11.527074	6.744965
"urine human chorionic gonadotropin abnormal"	38.209513	135.402036	10.782459
"superficial spreading melanoma stage i"	3.793852	8.393252	1.714868
"5q minus syndrome"	1.931603	3.210935	1.161994
