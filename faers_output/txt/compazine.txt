PT	PRR	high	low
"vaginoplasty"	1.081496	2.925322	0.399831
"hepatic vein stenosis"	1.189031	2.682657	0.527013
"eustachian tube patulous"	2.977411	6.441833	1.376157
"mucoepidermoid carcinoma"	1.050097	1.786946	0.617089
"hepatic infection bacterial"	1.030159	2.184441	0.485812
"femoral anteversion"	2.076086	5.117740	0.842194
"vaginal cellulitis"	1.321145	3.226713	0.540930
"colporrhaphy"	1.223798	3.316750	0.451551
"endocardial fibroelastosis"	1.279935	3.471584	0.471898
"ovarian cancer stage i"	3.617002	6.271002	2.086223
"intrapericardial thrombosis"	1.409222	3.829067	0.518640
"tibial torsion"	1.543285	3.779584	0.630156
"vitamin b1 deficiency"	1.092571	1.897535	0.629086
"keratosis pilaris"	1.876856	3.354805	1.050013
"vocal cord atrophy"	1.260659	2.846561	0.558309
