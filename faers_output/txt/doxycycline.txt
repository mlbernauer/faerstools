PT	PRR	high	low
"mediastinal cyst"	1.806173	4.402231	0.741048
"epiphyseal injury"	16.363932	34.465410	7.769478
"congenital knee deformity"	1.648759	2.927838	0.928469
"primary mediastinal large b-cell lymphoma stage ii"	1.298725	3.500663	0.481819
"abortion incomplete"	9.530521	10.679128	8.505453
"amblyopia strabismic"	5.244850	13.130380	2.095023
"visual evoked potentials abnormal"	1.090929	2.643908	0.450139
"primary hypogonadism"	1.411790	2.568629	0.775959
"retained products of conception"	1.976320	4.823412	0.809767
"biopsy thymus gland abnormal"	2.171003	4.400725	1.071017
"breast prosthesis removal"	1.295640	2.425612	0.692066
"wound infection fungal"	1.449739	2.925474	0.718429
"pelvic inflammatory disease"	1.046454	1.430722	0.765395
"social fear"	1.032425	2.315391	0.460354
"spina bifida occulta"	1.870164	4.219680	0.828857
"hypnopompic hallucination"	2.537044	6.913686	0.930993
"periodontal destruction"	1.401391	2.430164	0.808133
"wernicke-korsakoff syndrome"	3.519125	9.672659	1.280335
"lumbar spine flattening"	1.048970	1.672209	0.658015
"cardiac pseudoaneurysm"	1.219888	2.579950	0.576805
"laryngocele"	1.792606	3.052580	1.052695
"endometritis"	3.114534	4.337455	2.236409
"gingival cyst"	1.251065	3.035867	0.515557
"infectious disease carrier"	1.268522	3.078655	0.522679
"hypnagogic hallucination"	2.400043	4.389711	1.312207
"vaginal erosion"	1.087821	2.298598	0.514815
"jejunectomy"	3.162112	8.664358	1.154033
"mucosal necrosis"	1.136384	2.755067	0.468725
"pneumothorax spontaneous tension"	1.022746	1.977588	0.528932
"chondroplasty"	1.012202	1.957078	0.523512
"rocky mountain spotted fever"	1.274449	3.093187	0.525097
"light chain analysis"	1.055738	2.041781	0.545887
"protein urine absent"	1.044506	2.019925	0.540116
"jarisch-herxheimer reaction"	3.545518	6.212909	2.023320
"pancreaticoduodenectomy"	1.444939	3.899833	0.535369
"giant cell tumour of tendon sheath"	1.033511	1.998532	0.534465
"labia enlarged"	1.735568	3.683271	0.817805
"faecaluria"	1.759563	4.762238	0.650127
"biopsy lymph gland"	1.012325	1.635504	0.626597
"haemangioma of spleen"	1.352391	2.620105	0.698049
