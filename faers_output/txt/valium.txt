PT	PRR	high	low
"joint dislocation postoperative"	1.109238	2.706643	0.454589
"burn debridement"	3.684928	8.066787	1.683284
"hair plucking"	1.458154	2.749267	0.773374
"anal polyp"	1.342042	2.859986	0.629750
"von willebrand's factor multimers abnormal"	1.893822	4.671339	0.767780
"rectal cramps"	3.294102	7.179420	1.511419
"laryngeal repair"	1.634667	3.714979	0.719287
"ligament calcification"	2.389129	4.039299	1.413101
"corynebacterium test positive"	2.016797	4.983053	0.816261
"carcinoma in situ of eye"	1.158906	2.829783	0.474617
"cervix carcinoma stage iv"	2.740471	5.417604	1.386256
"csf shunt removal"	1.931109	3.459743	1.077879
"stiff-man syndrome"	1.489115	3.178857	0.697566
"cancer in remission"	1.230047	3.340943	0.452870
"vaginal polyp"	1.150321	2.808488	0.471157
