PT	PRR	high	low
"radiation exposure"	1.845193	5.001566	0.680734
"vulvovaginal human papilloma virus infection"	2.527356	5.138187	1.243148
"closed fracture manipulation"	1.256065	2.532506	0.622980
"ear canal erythema"	1.143130	2.772742	0.471283
"respiratory depth decreased"	1.692426	3.176906	0.901602
"intraocular melanoma"	1.306434	2.446806	0.697550
"ebstein's anomaly"	1.464919	2.599844	0.825430
"pseudohyperkalaemia"	1.797473	4.383619	0.737041
"kyphosis congenital"	5.392418	12.497117	2.326791
"urine phosphate increased"	1.974497	4.822385	0.808446
"joint dislocation postoperative"	1.861668	4.542586	0.762959
"spondylopathy traumatic"	1.039934	2.093761	0.516517
"conjunctivochalasis"	1.027851	1.744364	0.605652
"parachute mitral valve"	2.650511	6.019336	1.167107
"benign oesophageal neoplasm"	1.353941	2.401458	0.763351
"pancreatectomy"	1.675501	3.253795	0.862779
"genital hypoaesthesia"	1.591655	4.304223	0.588577
"thalassaemia"	1.063810	2.387183	0.474070
"bartholin's cyst removal"	1.118599	2.712688	0.461263
"periodontal destruction"	1.024100	1.914845	0.547711
"benign pancreatic neoplasm"	1.595716	2.833955	0.898500
"oesophageal dilation procedure"	1.259099	2.357498	0.672463
"colon injury"	3.169867	4.674499	2.149548
"vaginal fistula"	1.013063	1.839715	0.557857
"intravenous catheter management"	1.113026	2.498556	0.495817
"prostate tenderness"	1.483682	2.478605	0.888126
