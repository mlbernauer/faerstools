PT	PRR	high	low
"iliac vein occlusion"	1.164870	2.348185	0.577860
"right ventricle outflow tract obstruction"	1.272376	2.314864	0.699367
"congenital pulmonary artery anomaly"	1.211180	2.442320	0.600641
"arrested labour"	1.034989	1.833145	0.584352
"pulmonary coccidioides"	1.664100	4.506827	0.614452
"testicular cyst"	1.698769	3.608467	0.799735
"coronary bypass thrombosis"	1.488932	4.025547	0.550712
"primary mediastinal large b-cell lymphoma stage ii"	1.178738	3.177251	0.437303
"retinal anomaly congenital"	1.135481	2.755396	0.467925
"thrombophlebitis septic"	1.273492	2.569076	0.631270
"joint dislocation postoperative"	1.768107	4.314296	0.724614
"thyroid pain"	1.820110	4.443196	0.745589
"bed rest"	1.587893	2.136459	1.180180
"urethral prolapse"	2.869970	7.863893	1.047411
"chronic recurrent multifocal osteomyelitis"	15.633785	39.144515	6.243920
"product contamination physical"	1.147513	2.427615	0.542420
"preauricular cyst"	1.717005	3.874723	0.760856
"high-pitched crying"	2.936855	6.295106	1.370131
"nasal flaring"	1.650233	3.337758	0.815898
"urine uric acid decreased"	1.661308	4.049935	0.681479
"hypertrophic scar"	1.138092	2.556484	0.506654
"retrosternal infection"	4.125582	11.440941	1.487677
"periodontal destruction"	1.271919	2.205655	0.733468
"speech rehabilitation"	1.042252	2.099225	0.517472
"nail ridging"	1.833592	4.141654	0.811767
"adenoiditis"	1.140714	2.768221	0.470059
"ovarian low malignant potential tumour"	2.152478	5.857199	0.791020
"chest expansion decreased"	1.269410	3.424689	0.470525
"lice infestation"	1.327977	1.975486	0.892703
"sacralisation"	1.659452	3.743131	0.735689
"faecaluria"	2.434770	5.525797	1.072805
"culdoplasty"	1.010347	2.718872	0.375450
