PT	PRR	high	low
"persistent left superior vena cava"	1.552237	3.553504	0.678046
"insomnia related to another mental condition"	1.435087	3.083577	0.667885
"epilepsy congenital"	2.469467	6.227607	0.979232
"pectus carinatum"	1.448754	3.582197	0.585922
"bell's phenomenon"	108.656564	930.069342	12.693945
"apert's syndrome"	2.556625	7.204612	0.907243
"cerebral atrophy congenital"	2.287507	5.305138	0.986343
"dyskinesia neonatal"	1.131839	2.781267	0.460603
"hemivertebra"	1.196219	2.138946	0.668993
"mitral valve atresia"	1.943451	3.005961	1.256504
"microgenia"	3.104473	7.923959	1.216280
"congenital myopathy"	2.716414	7.680990	0.960671
"fever neonatal"	1.143753	2.599333	0.503272
"circumcision"	1.025062	2.513594	0.418028
"lactescent serum"	1.278313	2.607989	0.626568
"acrochordon excision"	1.580459	4.361059	0.572762
"dubin-johnson syndrome"	2.311842	5.812535	0.919498
"vaccination site inflammation"	2.173131	6.073837	0.777515
"hypochondroplasia"	4.346263	12.715754	1.485559
"stereotypic movement disorder"	3.104473	7.923959	1.216280
"hypokinesia neonatal"	1.640099	4.531503	0.593605
