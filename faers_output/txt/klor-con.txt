PT	PRR	high	low
"neurofibromatosis"	1.171179	2.839747	0.483022
"diffuse vasculitis"	3.233818	5.787288	1.806991
"abdominal panniculectomy"	2.285865	4.867669	1.073446
"staphyloma"	1.372832	2.254299	0.836032
"blood human chorionic gonadotropin abnormal"	1.701575	3.438440	0.842055
"chloracne"	5.551388	15.515702	1.986240
"oral neoplasm benign"	1.492309	2.796477	0.796354
"colon cancer stage i"	1.097112	2.658618	0.452737
"tracheal deviation"	1.132936	1.806548	0.710496
"laryngitis allergic"	4.223882	9.108371	1.958767
"os trigonum syndrome"	1.014614	2.142598	0.480464
"cholangiectasis acquired"	2.602213	5.900475	1.147622
"ventricular septal defect repair"	1.444882	2.278308	0.916330
"non-hodgkin's lymphoma stage iii"	1.203915	2.330109	0.622036
"bronchoalveolar lavage"	1.178639	2.204863	0.630057
"cardiac ventriculogram left"	2.250563	5.090469	0.995003
"prostatic acid phosphatase increased"	1.028035	2.763926	0.382375
"human chorionic gonadotropin increased"	1.843634	3.581525	0.949033
"exposure to chemical pollution"	1.121492	2.516410	0.499817
"retroperitoneal cancer"	1.353997	3.650671	0.502184
