PT	PRR	high	low
"acquired tracheo-oesophageal fistula"	2.906295	7.228677	1.168478
"pericardial calcification"	1.576665	3.360632	0.739704
"gamma radiation therapy to brain"	2.325036	6.390648	0.845891
"carotid body tumour"	1.492022	2.598262	0.856776
"atrophy of tongue papillae"	1.623336	3.976990	0.662617
"swollen tear duct"	1.052206	2.843608	0.389343
"lip neoplasm benign"	1.766571	4.335275	0.719856
"radical neck dissection"	2.030313	5.558891	0.741546
"morphoea"	1.427815	2.129289	0.957435
"cyst drainage"	1.045959	1.904806	0.574353
"gingival cancer"	1.386079	3.762669	0.510599
"metastases to nasal sinuses"	1.201269	2.087435	0.691301
"nail toxicity"	3.076420	6.656041	1.421920
"mucosal necrosis"	1.216475	2.460817	0.601349
"intravenous catheter management"	1.166700	2.265907	0.600726
"biopsy bladder abnormal"	1.390192	2.419110	0.798903
"cholangiogram"	1.298669	2.629130	0.641482
"sarcoma of skin"	1.264493	3.427041	0.466567
"stomatitis haemorrhagic"	1.251321	3.052026	0.513038
"leiomyosarcoma metastatic"	1.047618	2.548960	0.430569
"pharyngeal necrosis"	4.620264	11.721967	1.821097
"bacterial culture"	6.267488	18.122625	2.167534
"sphingomonas paucimobilis infection"	1.100399	2.975770	0.406912
"carotid arterial embolus"	1.217502	2.286359	0.648328
"epiploic appendagitis"	1.201269	2.928191	0.492812
