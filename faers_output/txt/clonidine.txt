PT	PRR	high	low
"sydenham's chorea"	2.074229	4.441524	0.968682
"paranasal sinus benign neoplasm"	1.396427	2.300445	0.847666
"thrombophlebitis migrans"	1.636350	2.993412	0.894511
"retinal anomaly congenital"	1.725046	3.252463	0.914933
"penile vascular disorder"	1.580365	4.299462	0.580899
"reticulocyte count abnormal"	1.443494	2.636608	0.790286
"blood erythropoietin decreased"	1.080691	2.026733	0.576244
"withdrawal hypertension"	3.417999	7.053385	1.656327
"pituitary enlargement"	1.142740	1.984543	0.658013
"glucose tolerance decreased"	1.189110	2.232071	0.633484
"herpangina"	3.025934	6.539795	1.400086
"nuclear magnetic resonance imaging brain"	1.082966	1.842573	0.636510
"urine uric acid decreased"	1.489601	3.369316	0.658564
"hepatoblastoma"	1.009436	1.891980	0.538569
"thrombosed varicose vein"	1.389460	2.703258	0.714175
"angiolipoma"	1.203666	1.900829	0.762200
"laryngotracheal oedema"	1.245542	2.339095	0.663237
"haemangioma of spleen"	1.015013	1.902522	0.541519
