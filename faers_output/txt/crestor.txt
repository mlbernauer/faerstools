PT	PRR	high	low
"rib hypoplasia"	1.381286	2.423900	0.787141
"central nervous system abscess"	1.123244	3.076588	0.410089
"radioisotope scan"	1.541707	4.265612	0.557214
"anal injury"	1.105693	2.163903	0.564978
"hepatic cancer stage iv"	1.709284	4.748291	0.615306
"pancreatitis viral"	2.268088	4.521839	1.137640
"gastric hypermotility"	1.209647	3.320189	0.440712
"heat oedema"	1.123244	3.076588	0.410089
"biliary polyp"	2.457095	6.225652	0.969749
"blood creatine"	1.268178	3.485773	0.461383
"blood creatine phosphokinase"	1.182362	1.958178	0.713918
"cerebellar haemangioma"	1.665827	4.150527	0.668585
"low density lipoprotein decreased"	1.500886	1.943058	1.159336
"high density lipoprotein"	1.615624	3.714001	0.702811
"neck exploration"	2.016078	5.641634	0.720460
"cancer in remission"	2.300259	4.295588	1.231774
