PT	PRR	high	low
"atelectasis neonatal"	2.041708	3.643405	1.144142
"myocardial oedema"	1.278615	3.112156	0.525313
"neonatal tachycardia"	1.154209	2.805817	0.474799
"diarrhoea neonatal"	1.256051	3.394843	0.464724
"dissociative identity disorder"	1.154209	2.002263	0.665346
"anxiety disorder due to a general medical condition"	1.779405	4.026585	0.786345
"persistent left superior vena cava"	1.986313	5.412924	0.728892
"schizoid personality disorder"	1.228942	3.320554	0.454833
"mitral valve atresia"	2.177155	3.801020	1.247034
"tibial torsion"	1.498447	4.061090	0.552892
"pulmonary vein stenosis"	2.001831	4.539155	0.882836
"hypoplastic right heart syndrome"	1.053280	1.915689	0.579112
"carcinoid tumour of the stomach"	1.240415	2.629802	0.585074
"child abuse"	1.020854	2.293975	0.454296
"occupational physical problem"	1.958978	4.800958	0.799340
"congenital megacolon"	1.538945	4.172754	0.567575
"deja vu"	1.083116	2.293223	0.511568
"poor weight gain neonatal"	1.952262	3.965310	0.961167
"congenital brain damage"	1.593497	3.890931	0.652603
"dementia of the alzheimer's type, with delusions"	3.163387	8.734401	1.145702
"pectus carinatum"	2.847048	7.039544	1.151450
"cerebral arteriovenous malformation haemorrhagic"	1.022892	2.757372	0.379458
"antidepressant drug level increased"	1.206067	1.842355	0.789531
"andropause"	2.162315	5.904183	0.791914
"school refusal"	1.186270	3.203710	0.439252
"meconium plug syndrome"	4.495340	12.595105	1.604439
"gross motor delay"	1.248705	3.038438	0.513180
"tubo-ovarian abscess"	1.211510	2.446262	0.600000
"acrochordon excision"	3.105871	8.570132	1.125588
"potter's syndrome"	1.047993	2.825842	0.388659
"carboxyhaemoglobin increased"	2.308418	4.701953	1.133315
"drug withdrawal syndrome"	1.057274	1.108319	1.008581
"primary hypogonadism"	1.105325	2.011062	0.607511
"univentricular heart"	1.021668	2.480284	0.420841
"oral bacterial infection"	1.567183	4.250671	0.577806
