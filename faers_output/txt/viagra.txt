PT	PRR	high	low
"radiation exposure"	1.961408	5.316570	0.723610
"radial tunnel syndrome"	4.262292	10.584625	1.716370
"iris transillumination defect"	5.277123	14.716341	1.892320
"paraesthesia of genital male"	3.121678	8.546908	1.140164
"fracture of penis"	14.667297	30.579023	7.035202
"optic ischaemic neuropathy"	7.679542	8.323329	7.085550
"retinitis pigmentosa"	1.455207	2.523871	0.839039
"chromatopsia"	1.716654	2.250367	1.309521
"haematospermia"	1.377591	2.198904	0.863047
"arteriovenous fistula, acquired"	1.978921	4.467578	0.876566
"ejaculation disorder"	2.772965	3.351962	2.293981
"male orgasmic disorder"	4.569880	7.608274	2.744880
"urethral repair"	9.234965	26.615417	3.204330
"connective tissue inflammation"	1.752088	3.541686	0.866766
"drug effect delayed"	1.556190	2.199230	1.101170
"heart and lung transplant"	1.910682	5.176789	0.705207
"painful erection"	7.577407	9.570507	5.999379
"peyronie's disease"	3.191808	4.244161	2.400389
"visual brightness"	1.229507	2.050761	0.737135
"fundoscopy abnormal"	1.787413	4.837612	0.660418
"urine output"	1.041537	2.522887	0.429984
"penile haemorrhage"	3.064701	4.073885	2.305512
"flatback syndrome"	1.420764	3.451702	0.584804
"palmar fasciitis"	6.272806	14.591437	2.696657
"penile curvature"	10.554246	18.013262	6.183894
"arteriosclerotic retinopathy"	1.958932	4.162655	0.921867
"penile vascular disorder"	9.346230	16.464593	5.305446
"genital injury"	5.215039	10.765520	2.526272
"orbital cyst"	4.818243	13.384559	1.734496
"choroidal infarction"	8.108750	19.098833	3.442714
"genital pain"	1.568202	2.298730	1.069832
"priapism"	1.337340	1.703847	1.049671
"triple vessel bypass graft"	1.235553	2.140629	0.713151
"penile pain"	4.566435	5.353680	3.894953
"optic nerve cup/disc ratio increased"	5.634894	13.050315	2.433047
"performance fear"	2.670351	7.282819	0.979123
"penile discharge"	2.570557	5.014812	1.317650
"testicular disorder"	3.222234	4.272911	2.429911
"encopresis"	1.385245	2.683932	0.714960
"reticulocytopenia"	1.164770	2.462167	0.551015
"penile erythema"	6.548430	11.634872	3.685638
"penis disorder"	7.198251	8.114762	6.385253
"product counterfeit"	1.482300	2.286264	0.961050
"penile operation"	4.104429	11.332631	1.486534
"counterfeit drug administered"	1.351458	3.037439	0.601309
"optic nerve infarction"	1.380315	2.921959	0.652052
"optic nerve cup/disc ratio decreased"	6.926224	19.584414	2.449528
"antidiuretic hormone abnormality"	1.121656	2.718661	0.462769
"andropause"	2.805559	7.660505	1.027499
"spontaneous penile erection"	3.668244	5.093348	2.641880
"orgasm abnormal"	1.379937	2.392453	0.795931
"ejaculation failure"	2.150738	2.704277	1.710502
"semen discolouration"	1.319281	3.556069	0.489445
"chorioretinopathy"	2.032953	2.755850	1.499682
"pigment dispersion syndrome"	5.683055	15.902733	2.030916
"penile size reduced"	3.996772	6.160158	2.593145
"circumcision"	2.071394	5.620062	0.763457
"scrotal erythema"	1.590712	3.581269	0.706555
"penile swelling"	5.982296	7.790053	4.594046
"semen analysis abnormal"	4.029803	11.119498	1.460436
"androgens decreased"	1.797074	3.193173	1.011369
"optic disc disorder"	2.220429	3.400072	1.450059
"brain natriuretic peptide"	1.481545	3.601077	0.609533
"spermatozoa abnormal"	1.351458	3.643827	0.501242
"ocular vascular disorder"	1.071460	1.557239	0.737219
"penis deviation"	2.098856	5.126102	0.859365
"scrotal disorder"	1.405003	2.307477	0.855494
"cyanopsia"	14.351200	16.625023	12.388370
"genital swelling"	3.606554	5.550814	2.343302
"erection increased"	31.606233	34.324239	29.103456
"prostatectomy"	1.656581	2.396577	1.145075
"penile haematoma"	19.789211	54.940200	7.127984
"premature ejaculation"	5.305193	7.873166	3.574810
"anorgasmia"	1.335819	1.768178	1.009182
"blood testosterone abnormal"	1.346766	2.850307	0.636345
"prostatic operation"	3.313798	4.507338	2.436307
"blood oestrogen increased"	1.147849	2.085259	0.631843
"erectile dysfunction"	1.441062	1.530331	1.357001
"radical prostatectomy"	2.793771	6.343610	1.230397
"optic disc haemorrhage"	2.363451	4.605269	1.212937
"haemodilution"	1.104514	2.478051	0.492303
"penile prosthesis insertion"	1.545293	3.274760	0.729192
