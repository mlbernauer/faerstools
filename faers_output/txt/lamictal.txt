PT	PRR	high	low
"vaginal mucosal blistering"	1.391137	2.825866	0.684839
"illiteracy"	1.407313	3.190656	0.620728
"tobacco poisoning"	1.613719	4.411962	0.590234
"mucosal hyperaemia"	2.593477	5.969928	1.126667
"kaolin cephalin clotting time prolonged"	1.141782	3.098425	0.420751
"anti-thyroid antibody"	1.458180	3.976928	0.534656
"mucosa vesicle"	2.311316	4.266994	1.251978
"chordee"	1.039370	2.019906	0.534822
"hypotelorism of orbit"	2.298017	5.269968	1.002071
"atrophy of tongue papillae"	1.080615	2.929586	0.398599
"occupational physical problem"	1.387946	3.401528	0.566332
"tongue eruption"	1.096939	1.951238	0.616673
"oral mucosa erosion"	1.099418	1.393072	0.867665
"skin degenerative disorder"	1.280114	2.340246	0.700221
"allergy to arthropod bite"	1.066334	2.158198	0.526860
"spina bifida occulta"	1.217245	2.591233	0.571807
"corneal leukoma"	5.673230	13.567834	2.372195
"monocyte percentage abnormal"	4.482552	12.810484	1.568503
"keratoconus"	1.015343	2.475230	0.416495
"pigmentation lip"	1.458180	3.976928	0.534656
"trisomy 13"	1.287542	3.502064	0.473368
"symblepharon"	1.163740	2.125127	0.637275
"normal delivery"	2.493105	3.850645	1.614164
"infective aneurysm"	1.952079	5.365546	0.710200
"epileptic aura"	1.023741	1.989202	0.526867
"anal cancer metastatic"	1.861983	5.110668	0.678381
"anticonvulsant drug level above therapeutic"	1.003002	1.363905	0.737597
"galactocele"	9.509415	18.722978	4.829839
"poland's syndrome"	2.881641	8.036148	1.033313
"borderline ovarian tumour"	1.287542	3.502064	0.473368
"motor developmental delay"	1.229200	2.140144	0.705996
"eyelid erosion"	2.180701	4.468828	1.064140
"retinal pigmentation"	1.053734	2.238753	0.495971
"holoprosencephaly"	1.613719	3.286170	0.792439
"radical prostatectomy"	1.000239	2.708215	0.369423
