PT	PRR	high	low
"unstable foetal lie"	5.468903	16.159318	1.850877
"skeletal survey abnormal"	1.073894	1.914171	0.602479
"arteriogram carotid abnormal"	1.158121	3.156865	0.424866
"paraesthesia of genital male"	1.386483	3.796139	0.506392
"oculoauriculovertebral dysplasia"	1.937800	3.688719	1.017988
"diverticular hernia"	1.047237	2.370624	0.462623
"bacterial tracheitis"	2.327979	5.053297	1.072466
"mandibulofacial dysostosis"	2.895302	8.158997	1.027427
"spinal myelogram"	1.097847	1.957394	0.615751
"eagle barrett syndrome"	1.246079	3.402440	0.456353
"tooth decalcification"	1.351141	2.313924	0.788955
"femoral artery embolism"	1.828176	3.211478	1.040713
"abnormal palmar/plantar creases"	1.428263	2.498258	0.816543
"shone complex"	1.052837	2.136357	0.518858
"laryngitis allergic"	1.872505	4.037949	0.868331
"facet joint block"	1.266695	2.707511	0.592616
"cholangiectasis acquired"	1.153597	2.615812	0.508747
"venous bruit"	1.968805	5.451332	0.711055
"nasogastric output high"	1.171908	3.195303	0.429808
"wound closure"	1.498786	1.993937	1.126595
"pityriasis rubra pilaris"	1.054717	2.387841	0.465872
"pulmonary arteriovenous fistula"	1.714134	2.946599	0.997168
"pierre robin syndrome"	1.432051	2.110861	0.971533
"myocarditis post infection"	2.400982	6.703016	0.860018
"occupational exposure to drug"	4.006290	8.905647	1.802267
"laryngeal repair"	1.295267	2.943662	0.569942
"high-pitched crying"	1.459919	3.129368	0.681085
"foetal damage"	3.038280	5.860439	1.575162
"velopharyngeal incompetence"	1.060779	2.596642	0.433349
