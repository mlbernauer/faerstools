PT	PRR	high	low
"thromboembolectomy"	1.088809	2.651904	0.447039
"neurological eyelid disorder"	1.629370	4.441398	0.597750
"cranial sutures widening"	2.541220	5.209913	1.239521
"renal cancer stage iii"	7.526982	19.798543	2.861598
"endoscopy biliary tract"	2.826459	7.832033	1.020025
"diabetic ketoacidotic hyperglycaemic coma"	1.000697	2.434622	0.411314
"neonatal anuria"	1.527535	2.977897	0.783560
"hypospermia"	6.595070	19.212731	2.263861
"jealous delusion"	1.228598	2.241620	0.673376
"urethral prolapse"	2.007195	5.499879	0.732531
"vocal cord polypectomy"	4.438990	11.262069	1.749646
"megakaryocytes increased"	1.120349	1.730084	0.725503
"venipuncture site swelling"	5.969676	15.421652	2.310844
"oesophagitis haemorrhagic"	1.310935	1.846200	0.930859
"body mass index decreased"	1.569859	2.061886	1.195245
"naevus flammeus"	5.246079	7.989177	3.444828
"alkalosis hypokalaemic"	1.272946	3.107502	0.521445
"asthma late onset"	1.398954	3.801170	0.514861
"haemorrhagic urticaria"	3.919712	9.117919	1.685049
"myocardial strain"	1.042895	2.538623	0.428433
"breast abscess"	1.658056	2.022260	1.359445
"podagra"	3.957042	11.133568	1.406394
"blood folate abnormal"	1.648768	4.495478	0.604704
"polyneuropathy in malignant disease"	1.202226	2.932283	0.492909
"epiglottic oedema"	1.810411	2.728957	1.201041
"endometrial cancer stage ii"	1.065358	2.400487	0.472815
"bronchial injury"	1.556140	4.237489	0.571464
"stool ph decreased"	6.658484	17.339557	2.556894
"nitritoid reaction"	3.763491	9.471387	1.495437
