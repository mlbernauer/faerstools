PT	PRR	high	low
"selective iga immunodeficiency"	6.940979	11.522484	4.181146
"congenital knee deformity"	1.530327	2.717538	0.861773
"multiple chemical sensitivity"	1.436264	3.879951	0.531670
"cystic fibrosis lung"	1.082959	2.915659	0.402242
"adrenal cyst"	1.100616	2.471078	0.490213
"neurosensory hypoacusis"	1.259981	2.291767	0.692719
"spondylopathy traumatic"	1.010041	2.033577	0.501669
"wound infection fungal"	1.693255	3.179413	0.901774
"spinal cord injury thoracic"	1.095324	2.119909	0.565937
"spina bifida occulta"	1.438304	3.498512	0.591314
"vessel puncture site reaction"	3.214496	6.568325	1.573154
"gingival cyst"	1.399861	3.150289	0.622042
"infectious disease carrier"	1.177403	2.857519	0.485133
"pigmentary glaucoma"	1.446523	3.256483	0.642543
"skin cyst excision"	1.165784	2.466128	0.551087
"jejunectomy"	2.934975	8.042005	1.071136
"thyroglobulin increased"	1.036757	2.326543	0.462001
"gastric varices haemorrhage"	1.878285	2.874422	1.227361
"erysipeloid"	1.144143	3.082191	0.424718
"tendon pain"	1.165696	1.439826	0.943758
"carcinoma in situ of skin"	1.252291	2.650784	0.591610
"pancreaticoduodenectomy"	1.341147	3.619712	0.496912
"myofascial spasm"	1.311614	3.186902	0.539813
"stag horn calculus"	1.069612	2.069806	0.552743
"vagotomy"	1.016633	2.463940	0.419467
