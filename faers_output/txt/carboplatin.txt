PT	PRR	high	low
"acquired tracheo-oesophageal fistula"	3.477576	8.043866	1.503448
"lymph node abscess"	1.414214	3.841886	0.520578
"metastases to thyroid"	1.209879	2.351164	0.622588
"miller fisher syndrome"	1.212184	2.735102	0.537234
"recall phenomenon"	1.624722	2.183274	1.209065
"urogenital fistula"	1.911100	3.892689	0.938247
"acute myelomonocytic leukaemia"	1.001350	1.737706	0.577026
"laryngeal necrosis"	10.398635	28.185287	3.836456
"retinoblastoma"	2.828429	6.496455	1.231442
"subdural effusion"	1.099130	2.476895	0.487743
"swollen tear duct"	1.032273	2.789740	0.381967
"myocardial strain"	1.064920	2.592236	0.437482
"cytokine storm"	1.967603	2.808798	1.378333
"laryngeal haemorrhage"	1.172927	2.490481	0.552406
"radiation mucositis"	1.130080	1.923971	0.663773
"choroidal dystrophy"	4.329228	7.883740	2.377325
"cerebellar tumour"	1.111804	2.707909	0.456480
"burkitt's lymphoma recurrent"	1.321696	3.586033	0.487134
"iris neovascularisation"	6.963934	12.622132	3.842170
"radiation dysphagia"	4.057172	8.869552	1.855860
"histiocytic necrotising lymphadenitis"	2.357024	5.065644	1.096714
"pericarditis uraemic"	1.885619	5.155333	0.689686
"ewing's sarcoma"	1.296651	2.170625	0.774572
"cholangiectasis acquired"	1.657282	3.757908	0.730881
"leprosy"	1.522480	2.967039	0.781231
"paraneoplastic cerebellar degeneration"	5.356873	13.721482	2.091325
"radiation pneumonitis"	1.596111	1.938853	1.313957
"dysponesis"	2.783887	5.299249	1.462476
"drug clearance increased"	1.104855	2.988854	0.408419
"foetal-maternal haemorrhage"	2.318384	6.375568	0.843047
"ovarian epithelial cancer recurrent"	11.313715	25.081943	5.103279
"cholangiogram"	1.274067	2.579325	0.629330
"radiation myelopathy"	2.685217	6.157901	1.170917
"sphingomonas paucimobilis infection"	1.079553	2.919399	0.399204
"pyomyositis"	1.653308	2.883244	0.948039
"extraocular muscle disorder"	2.051874	3.046797	1.381840
"endocarditis noninfective"	1.619329	3.670307	0.714444
"mucosal necrosis"	1.039863	2.204918	0.490411
"radiation hepatitis"	7.071072	20.687528	2.416918
