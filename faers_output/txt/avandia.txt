PT	PRR	high	low
"stent placement"	1.234109	1.327137	1.147601
"cardiovascular disorder"	1.135616	1.181728	1.091303
"myocardial infarction"	1.028816	1.043792	1.014054
"cardiac assistance device user"	8.421351	10.818553	6.555328
"heart injury"	3.632883	3.835685	3.440803
"silent myocardial infarction"	1.204109	1.450167	0.999801
"bladder cancer stage iv"	2.090775	5.261793	0.830770
"coronary artery bypass"	2.018051	2.155945	1.888976
"macular oedema"	1.068774	1.184644	0.964237
