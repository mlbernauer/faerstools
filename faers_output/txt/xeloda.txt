PT	PRR	high	low
"tumour marker increased"	1.092393	1.354889	0.880754
"biopsy bladder abnormal"	1.558677	2.712284	0.895730
"retroperitoneal neoplasm metastatic"	1.154449	3.118971	0.427305
"keratosis obturans"	1.063308	2.391655	0.472737
"extravasation of urine"	3.513539	9.760307	1.264813
"gamma radiation therapy to brain"	2.606819	7.165149	0.948411
"biliary fistula"	1.110047	2.698985	0.456543
"urethral ulcer"	5.050713	12.797071	1.993401
"carotid body tumour"	1.539265	2.739425	0.864903
"pericardial effusion malignant"	1.222357	1.953551	0.764842
"eustachian tube patulous"	3.449267	7.462700	1.594255
"collagen-vascular disease"	1.094017	1.827028	0.655093
"oesophageal mass"	1.092046	1.894430	0.629511
"metastases to eye"	1.003868	1.648504	0.611313
"bacteria tissue specimen identified"	1.004348	1.741281	0.579295
"chronic granulomatous disease"	1.087846	2.304556	0.513508
"gastrooesophageal cancer"	1.924081	4.364132	0.848299
"n-terminal prohormone brain natriuretic peptide increased"	2.145436	4.876619	0.943870
"perineal ulceration"	1.584537	3.211327	0.781845
"vena cava injury"	1.070350	2.159849	0.530430
"palmar-plantar erythrodysaesthesia syndrome"	1.448938	1.540914	1.362452
"metastases to nasal sinuses"	1.240073	2.202412	0.698225
"cholangiectasis acquired"	1.894017	4.294693	0.835287
"dihydropyrimidine dehydrogenase deficiency"	12.626782	16.362994	9.743671
"skin chapped"	1.854220	2.287568	1.502964
"mucosal necrosis"	1.363906	2.759048	0.674232
"benign breast lump removal"	1.208718	2.563417	0.569942
"sphingomonas paucimobilis infection"	1.233762	3.336412	0.456229
"vocal cord atrophy"	1.460447	3.297669	0.646792
"omentum neoplasm"	1.690615	3.182603	0.898064
