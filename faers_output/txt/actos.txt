PT	PRR	high	low
"malignant neoplasm of renal pelvis"	1.292731	3.161084	0.528664
"retinal microaneurysms"	1.800067	4.097203	0.790842
"bladder cancer stage 0, with cancer in situ"	3.044793	6.033358	1.536585
"anal injury"	1.788816	3.500781	0.914043
"bladder transitional cell carcinoma stage iii"	3.140857	6.493827	1.519132
"bladder cancer stage i, without cancer in situ"	4.386369	12.477032	1.542052
"neuroglycopenia"	1.261951	3.084537	0.516292
"bladder adenocarcinoma stage unspecified"	2.271513	6.264043	0.823712
"bladder transitional cell carcinoma stage i"	1.252015	3.059839	0.512295
"gastrooesophageal cancer"	1.252015	3.059839	0.512295
"bladder cancer"	4.112179	4.325581	3.909304
"bladder transitional cell carcinoma stage ii"	3.392125	7.031642	1.636391
"bladder transitional cell carcinoma stage 0"	5.782032	16.778726	1.992517
"preauricular cyst"	1.102931	2.488984	0.488736
"post-thoracotomy pain syndrome"	2.168262	4.957497	0.948333
"bladder cancer stage ii"	2.778743	5.491787	1.405993
"benign anorectal neoplasm"	1.089081	2.655777	0.446611
"diabetic cardiomyopathy"	1.039254	2.532560	0.426465
"bladder transitional cell carcinoma"	2.821996	3.264277	2.439641
"urine cytology abnormal"	1.060039	1.932876	0.581353
"retinal aneurysm"	1.120367	1.947847	0.644415
"metastatic carcinoma of the bladder"	1.739127	2.978194	1.015569
