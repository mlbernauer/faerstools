PT	PRR	high	low
"echinococciasis"	1.116041	2.371126	0.525298
"arteriovenous graft aneurysm"	1.016268	1.699164	0.607829
"hypofibrinogenaemia"	1.001448	2.438924	0.411205
"euthanasia"	1.308013	3.554778	0.481296
"hungry bone syndrome"	1.238505	2.509566	0.611219
"hy's law case"	1.294801	3.518174	0.476528
"reticulocyte count abnormal"	1.140439	2.142825	0.606956
"salmonella bacteraemia"	1.048244	2.225370	0.493768
"larynx irritation"	1.011989	2.280968	0.448986
"milk-alkali syndrome"	6.387612	8.446452	4.830618
"ear deformity acquired"	1.006691	2.268878	0.446664
"acute monocytic leukaemia"	1.057764	1.551915	0.720957
"ultrasound liver"	2.981054	8.304755	1.070071
"anisometropia"	1.158301	2.615441	0.512977
"omphalitis"	1.075380	2.621586	0.441123
"acne conglobata"	2.373802	6.554319	0.859729
"trigeminal nerve paresis"	3.433535	7.968629	1.479447
"biliary sphincterotomy"	1.576049	3.576938	0.694429
"oestrogen replacement therapy"	3.662437	10.304677	1.301685
