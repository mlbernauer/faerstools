PT	PRR	high	low
"lymphoblast count increased"	7.183530	20.390186	2.530781
"neurofibromatosis"	1.841931	3.726559	0.910413
"drain removal"	3.285151	8.103115	1.331860
"mesenteric panniculitis"	1.061605	2.855829	0.394634
"gene mutation identification test positive"	8.796159	11.455311	6.754283
"pseudoporphyria"	1.005694	2.124068	0.476172
"gastrointestinal stromal tumour"	6.323199	7.173008	5.574069
"urethral valves"	2.590215	6.353918	1.055918
"leukaemia recurrent"	1.300931	1.687793	1.002742
"bronchial polyp"	13.469118	40.287744	4.503036
"disorder of orbit"	1.394623	2.198994	0.884483
"acute lymphocytic leukaemia recurrent"	1.060734	1.588666	0.708240
"hemivertebra"	1.443120	3.246525	0.641484
"adrenal gland operation"	26.938237	89.458370	8.111802
"bladder neoplasm surgery"	1.939553	3.771244	0.997513
"chloroma"	3.009859	4.721734	1.918628
"typhoid fever"	1.620345	4.380832	0.599320
"tumour excision"	1.777650	2.445358	1.292261
"local anaesthesia"	1.436706	3.231944	0.638663
"bacteroides test positive"	1.579955	2.962936	0.842495
"gastrointestinal cancer metastatic"	1.301364	3.159593	0.536002
"rectal stenosis"	1.461765	3.096895	0.689967
"subdural haematoma evacuation"	1.117254	2.161720	0.577437
"blast cell crisis"	3.860308	5.892953	2.528779
"calcification of muscle"	1.417802	3.445515	0.583414
"spleen operation"	4.066149	11.234364	1.471696
"blast cell count increased"	1.158136	1.721045	0.779339
"basophilia"	1.726810	4.206872	0.708810
"allogenic bone marrow transplantation therapy"	2.649663	5.173744	1.356989
"colon cancer stage i"	1.282773	2.882408	0.570879
"pseudomyxoma peritonei"	2.494281	6.113923	1.017585
"amniorrhexis"	3.591765	9.882403	1.305429
"polymerase chain reaction"	3.305305	6.259003	1.745492
"ultrasound foetal"	12.165655	27.626609	5.357269
"gastric tube reconstruction"	71.835297	320.964973	16.077486
"exposure via partner"	25.142354	61.663276	10.251450
"herpetic stomatitis"	1.072169	2.884519	0.398522
"alveolar proteinosis"	2.442762	3.917303	1.523264
"episiotomy"	33.672796	102.928764	11.015941
"amniocentesis abnormal"	1.108569	2.687333	0.457303
"cytogenetic analysis abnormal"	1.436706	3.877945	0.532273
"leukaemic infiltration extramedullary"	2.477079	6.748551	0.909221
"eye oedema"	1.019615	1.348674	0.770843
"gastrectomy"	2.505882	3.440377	1.825221
"congenital bladder anomaly"	1.288911	3.129044	0.530926
"chronic myeloid leukaemia"	3.805754	4.294201	3.372865
"tensilon test abnormal"	7.981700	22.810276	2.792931
"leukaemic infiltration"	1.977118	5.362509	0.728949
"basilar artery stenosis"	1.355383	3.655768	0.502511
"trigeminal palsy"	8.506812	20.122894	3.596195
"bone marrow leukaemic cell infiltration"	5.437075	10.107331	2.924787
"gas gangrene"	1.066861	2.147861	0.529919
"mastocytosis"	1.065901	1.625380	0.699003
"stomatococcal infection"	3.367280	9.246438	1.226264
"stoma care"	41.903924	112.516321	15.606081
"haemorrhagic ascites"	1.235263	2.140492	0.712862
"central nervous system leukaemia"	3.591765	8.880856	1.452650
"exposure via father"	5.244612	9.739737	2.824097
"omentectomy"	1.203944	3.242885	0.446972
"pancreatectomy"	1.142257	2.564020	0.508870
"tumour ulceration"	1.728657	3.897021	0.766805
"finger hypoplasia"	1.924160	4.089306	0.905384
"gastrointestinal stoma necrosis"	3.035294	8.310402	1.108612
"chromosome analysis abnormal"	6.830767	8.742872	5.336848
"spleen palpable"	1.941495	5.264212	0.716043
"laryngeal mass"	1.252941	3.376351	0.464958
"eaton-lambert syndrome"	3.026768	7.450555	1.229617
"intestinal stoma"	1.550402	4.189104	0.573809
"chronic myeloid leukaemia transformation"	4.832773	7.164269	3.260025
"trisomy 8"	4.144344	8.163715	2.103894
"periorbital oedema"	1.603371	1.844483	1.393778
"therapy responder"	3.918289	6.508477	2.358922
"blast crisis in myelogenous leukaemia"	11.505194	13.476385	9.822329
"hyperkeratosis palmaris and plantaris"	1.032116	2.500460	0.426027
"bacterial food poisoning"	3.780805	10.419847	1.371852
"tongue operation"	11.342415	33.339996	3.858740
"leukostasis"	1.207993	2.930694	0.497918
"eosinophilic fasciitis"	1.766442	4.782078	0.652502
"mixed deafness"	1.737951	3.167930	0.953453
"orbital oedema"	1.517647	2.276801	1.011618
"fibrosarcoma"	2.798778	7.646800	1.024371
"gastric cancer stage i"	6.338409	17.861458	2.249280
"bifidobacterium test positive"	1.931056	3.628572	1.027671
"eosinophilic leukaemia"	9.620799	24.915399	3.714962
"hepatectomy"	2.209777	3.418555	1.428415
"leukaemic infiltration brain"	3.008331	5.521096	1.639177
"polyserositis"	1.410379	2.354798	0.844730
"gastric antral vascular ectasia"	1.715470	2.438938	1.206606
"hbv-dna polymerase increased"	11.972550	35.375569	4.052004
"oesophagectomy"	4.617983	10.629989	2.006189
"vascular fragility"	1.290454	3.478612	0.478718
"tumour haemorrhage"	1.073566	1.372165	0.839945
"angiosarcoma metastatic"	4.489706	12.450691	1.618983
"pancreaticoduodenectomy"	4.521103	8.148008	2.508634
"slit-lamp tests abnormal"	26.938237	78.811337	9.207668
"retinal operation"	1.885677	4.006478	0.887507
"transmission of drug via semen"	1.304966	1.898316	0.897078
"tumour perforation"	1.224465	2.750212	0.545163
"peritoneal lesion"	1.772252	4.319150	0.727198
"biliary drainage"	1.428543	3.025829	0.674438
"oesophagoenterostomy"	13.469118	35.886852	5.055254
"therapeutic embolisation"	1.082526	1.874396	0.625195
"signet-ring cell carcinoma"	1.231462	3.317829	0.457076
"cardiac pacemaker revision"	1.456121	3.931035	0.539371
"colectomy partial"	1.388569	3.373681	0.571519
"blast cells present"	2.300145	3.405555	1.553540
"desmoid tumour"	3.673396	7.216095	1.869964
"clonal evolution"	3.265241	8.958170	1.190176
"full blood count increased"	2.460113	4.636512	1.305325
"laparoscopic surgery"	1.608253	3.622396	0.714024
"chromosomal mutation"	2.014074	5.464546	0.742329
