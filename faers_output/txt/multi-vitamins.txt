PT	PRR	high	low
"atelectasis neonatal"	1.327991	2.275047	0.775175
"pulmonary aplasia"	2.016279	4.362390	0.931916
"hydromyelia"	1.724034	3.276173	0.907245
"pachygyria"	3.830151	9.074772	1.616576
"hemivertebra"	1.300143	2.324767	0.727114
"bacterial tracheitis"	2.588412	5.370795	1.247465
"tibial torsion"	1.717765	3.520938	0.838048
"aorta hypoplasia"	1.003819	1.630932	0.617838
"auricular pseudocyst"	2.999271	6.258874	1.437260
"scaphocephaly"	2.018740	3.850508	1.058383
"head lag"	1.124727	3.066662	0.412504
"vacterl syndrome"	1.655556	2.794403	0.980841
"congenital naevus"	1.523823	2.888365	0.803928
"pulmonary artery atresia"	1.050610	1.544713	0.714554
"shone complex"	1.548804	2.777417	0.863678
"penile torsion"	5.648085	10.904050	2.925597
"erythroid series abnormal"	3.107798	7.895753	1.223241
"congenital tongue anomaly"	1.026924	2.514177	0.419451
"gross motor delay"	1.272894	2.489251	0.650903
"maternal exposure timing unspecified"	1.792761	2.040676	1.574965
"apert's syndrome"	3.578676	9.166739	1.397108
"ischiorectal hernia"	1.349672	3.072702	0.592838
"femoral anteversion"	1.405909	3.465715	0.570324
"shwachman-diamond syndrome"	5.450599	13.242305	2.243494
"genital labial adhesions"	1.548804	4.259244	0.563197
"circumcision"	1.349672	3.072702	0.592838
"arachnodactyly"	2.624363	7.373088	0.934111
"staphylococcal pharyngitis"	1.085943	2.958587	0.398593
"laevocardia"	4.217726	10.923003	1.628600
"pulmonary oedema neonatal"	2.103085	3.708676	1.192600
