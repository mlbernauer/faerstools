PT	PRR	high	low
"univentricular heart"	1.398279	2.833485	0.690028
"central nervous system abscess"	2.057467	5.635400	0.751175
"iron binding capacity total increased"	1.620255	3.159542	0.830889
"azoospermia"	1.772300	2.808992	1.118212
"rectourethral fistula"	10.081590	23.308537	4.360568
"angle closure glaucoma"	1.091919	1.365729	0.873004
"ectopia cordis"	6.188476	11.732875	3.264096
"emotional poverty"	1.723676	3.364300	0.883114
"loss of bladder sensation"	1.605349	3.422808	0.752933
"factor xiii deficiency"	2.823975	7.813342	1.020669
"orgasmic sensation decreased"	1.551969	3.798984	0.634014
"cerebral vasoconstriction"	2.992915	4.268594	2.098476
"hepatitis c virus test positive"	2.217358	3.027884	1.623800
"haemangioma congenital"	1.164604	2.069339	0.655428
"anencephaly"	2.952847	4.258407	2.047551
"postmature baby"	1.268923	2.568215	0.626959
"tooth decalcification"	1.103622	2.230165	0.546140
"atonic urinary bladder"	1.278706	2.020565	0.809224
"cystoid macular oedema"	1.003254	1.945645	0.517319
"alkalosis hypokalaemic"	1.323738	3.231494	0.542252
"congenital pulmonary valve atresia"	1.613700	2.662730	0.977954
"abnormal palmar/plantar creases"	1.095825	2.324445	0.516610
"gender identity disorder"	3.190377	6.910549	1.472893
"optic nerve hypoplasia"	1.615639	3.445106	0.757681
"combined immunodeficiency"	4.629301	9.266841	2.312593
"camptodactyly congenital"	1.698381	4.164632	0.692618
"sympodia"	36.005678	143.968182	9.004829
"exaggerated startle response"	1.636622	3.191920	0.839160
"gross motor delay"	1.714556	3.484761	0.843588
"intraocular pressure fluctuation"	1.071598	2.608085	0.440293
"trisomy 18"	1.323043	2.255268	0.776158
"agonal rhythm"	1.055541	2.048004	0.544026
"amniotic fluid volume increased"	2.400379	6.604475	0.872411
"acquired epileptic aphasia"	2.571834	5.887596	1.123435
"choanal atresia"	2.919379	5.257382	1.621106
"intestinal strangulation"	1.000158	2.119460	0.471967
"diarrhoea neonatal"	1.058991	2.862240	0.391812
"nail ridging"	2.278840	4.317847	1.202709
"hypermobility syndrome"	1.033656	2.327162	0.459119
"osmotic demyelination syndrome"	1.748379	2.492860	1.226233
"vascular headache"	1.065958	2.068407	0.549343
"obstructed labour"	1.866961	3.991077	0.873334
