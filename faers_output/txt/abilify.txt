PT	PRR	high	low
"micrographia"	1.170028	2.631334	0.520256
"merycism"	1.747478	3.400843	0.897918
"lactation disorder"	1.059908	2.136606	0.525790
"foetal chromosome abnormality"	1.209291	3.264406	0.447979
"loose associations"	1.260031	3.403227	0.466521
"alpha 1 foetoprotein amniotic fluid increased"	7.949310	16.146310	3.913683
"parkinsonian rest tremor"	1.640028	2.745237	0.979766
"schizotypal personality disorder"	1.185423	2.880808	0.487790
"paraphilia"	2.002049	5.450397	0.735396
"blood prolactin decreased"	2.010986	4.925815	0.820994
"verbigeration"	1.225744	3.309404	0.453994
"oculogyric crisis"	1.424382	2.025040	1.001889
"exposure via father"	1.501536	4.066020	0.554501
"periumbilical abscess"	1.820044	4.945308	0.669839
"congenital ureteric anomaly"	3.624398	7.827901	1.678134
"abortion early"	3.833710	10.640428	1.381273
"delayed puberty"	2.953842	8.123040	1.074128
"ovarian low malignant potential tumour"	1.958526	5.329440	0.719742
"akathisia"	1.802934	1.966098	1.653311
"superior mesenteric artery syndrome"	1.453100	3.932819	0.536892
