PT	PRR	high	low
"actinomycotic pulmonary infection"	1.979475	4.856750	0.806778
"cryoglobulins present"	1.727846	4.228168	0.706087
"hepatitis c rna increased"	4.334582	6.445070	2.915189
"exposure via semen"	2.146167	5.274952	0.873190
"salmonella bacteraemia"	1.531537	3.102164	0.756119
"tongue pigmentation"	3.398098	8.461469	1.364665
"oral lichen planus"	1.036708	1.838525	0.584579
"hepatitis c virus test"	2.522299	5.752807	1.105894
"anal pruritus"	4.356082	5.030119	3.772365
"sudden visual loss"	1.102086	2.479572	0.489840
"hypercatabolism"	3.866801	7.175989	2.083636
"capsule physical issue"	1.822444	3.700355	0.897563
"peritonsillitis"	1.611746	3.031996	0.856771
"viral load increased"	1.346170	1.637943	1.106371
"exposure via father"	1.713327	4.191996	0.700260
"vogt-koyanagi-harada syndrome"	8.778420	12.939881	5.955283
"retinal exudates"	1.366246	1.800939	1.036474
"paternal drugs affecting foetus"	1.374512	2.443232	0.773272
"ocular sarcoidosis"	3.020531	8.339971	1.093962
"hepatitis c rna positive"	1.577018	2.689515	0.924696
"pregnancy of partner"	1.045569	1.717312	0.636584
"porphyria non-acute"	1.287700	1.933162	0.857751
"anorectal discomfort"	3.321019	3.687850	2.990676
"cutaneous sarcoidosis"	1.333505	2.105835	0.844432
"activated protein c resistance"	2.965613	8.183121	1.074756
"umbilical cord vascular disorder"	1.349245	2.621029	0.694560
