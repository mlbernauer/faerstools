PT	PRR	high	low
"secondary syphilis"	1.578187	4.288163	0.580826
"device material issue"	1.512005	3.417531	0.668951
"pseudoporphyria"	1.376102	2.392448	0.791515
"douglas' abscess"	2.441258	6.703664	0.889028
"schizotypal personality disorder"	1.027898	2.498000	0.422968
"hypoaldosteronism"	1.308234	2.778094	0.616061
"nasal flaring"	1.302004	2.633452	0.643724
"venous bruit"	3.124811	8.652078	1.128566
"xanthelasma"	1.627506	3.981423	0.665283
"ear canal injury"	2.693802	7.419883	0.977990
"pigmentary glaucoma"	1.502313	3.043908	0.741463
"diverticular fistula"	1.092591	2.950998	0.404526
"high-pitched crying"	2.317127	4.966762	1.081001
"nail ridging"	1.198164	2.917270	0.492103
"gliosarcoma"	2.693802	7.419883	0.977990
"urethral operation"	1.516898	4.118525	0.558690
"carcinoma in situ of skin"	1.108089	2.237441	0.548779
"duodenal scarring"	2.320404	5.287300	1.018341
"pseudoparalysis"	2.830445	7.015719	1.141924
"myofascial spasm"	1.644637	3.336405	0.810702
"superficial vein prominence"	1.587810	3.882620	0.649340
"intestinal diaphragm disease"	5.756230	10.088040	3.284502
