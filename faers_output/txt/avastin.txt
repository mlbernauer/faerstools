PT	PRR	high	low
"retinal cyst"	2.002799	4.910156	0.816920
"macular ischaemia"	1.648458	3.340010	0.813594
"detachment of retinal pigment epithelium"	6.890660	8.509895	5.579528
"extravasation of urine"	3.726948	10.353126	1.341638
"ileal perforation"	1.446278	1.906490	1.097158
"endophthalmitis"	3.472814	3.918484	3.077832
"pericardial calcification"	1.875121	3.996767	0.879731
"colonic fistula"	1.127892	1.677691	0.758269
"retinal pigment epithelial tear"	4.613625	5.913602	3.599419
"nasal septum perforation"	1.794136	2.417371	1.331580
"adrenal gland cancer metastatic"	1.284192	2.594431	0.635649
"hypopyon"	1.480248	2.264118	0.967765
"corneal striae"	4.559564	11.463726	1.813514
"anterior chamber inflammation"	1.020474	2.293034	0.454144
"plicated tongue"	3.018303	7.474878	1.218770
"small intestinal perforation"	1.252807	1.544777	1.016020
"portal vein pressure increased"	2.142995	5.261228	0.872882
"product sterility lacking"	8.036232	19.218937	3.360281
"sphenoid sinus operation"	1.728222	4.225359	0.706863
"ileal fistula"	1.344624	2.717830	0.665242
"vaginal laceration"	1.318766	2.971458	0.585283
"pulmonary fistula"	1.714396	4.657349	0.631079
"splenic necrosis"	1.260585	3.067573	0.518024
"meningoencephalitis bacterial"	1.587404	3.214748	0.783841
"chest wall necrosis"	3.968510	9.920339	1.587554
"nail toxicity"	3.658772	7.915965	1.691091
"renal salt-wasting syndrome"	1.017446	2.051159	0.504688
"counterfeit drug administered"	1.045364	2.349501	0.465114
"frontal sinus operation"	1.076882	2.615687	0.443354
"eastern cooperative oncology group performance status worsened"	1.954523	3.339254	1.144016
"vitritis"	4.220671	5.280550	3.373524
"rectal obstruction"	1.416856	3.836210	0.523298
"biliary fistula"	1.420770	3.204297	0.629963
"tumour perforation"	2.521171	4.243643	1.497841
"haemorrhagic tumour necrosis"	3.255183	7.464946	1.419463
"cytokine storm"	2.385247	3.404959	1.670917
"bronchial fistula"	1.008468	2.032915	0.500271
"rectal perforation"	2.297238	2.921437	1.806407
"intestinal stoma site bleeding"	6.122844	15.627966	2.398854
"mucosal exfoliation"	1.332432	3.002628	0.591274
"hypercreatinaemia"	1.001400	2.430490	0.412592
"brachiocephalic vein stenosis"	1.452878	3.935334	0.536385
"pseudoendophthalmitis"	6.408958	10.837516	3.790051
"choroidal neovascularisation"	1.253672	2.060908	0.762622
"tracheal fistula"	2.005142	4.074191	0.986845
"gastrointestinal perforation"	2.154317	2.404505	1.930161
"tachyphylaxis"	1.020474	2.293034	0.454144
"transmission of an infectious agent via a medicinal product"	1.169778	1.508446	0.907146
"cerebral amyloid angiopathy"	1.558542	4.226556	0.574712
"keratosis obturans"	1.127892	2.536918	0.501451
"omentum neoplasm"	1.607246	3.126642	0.826203
