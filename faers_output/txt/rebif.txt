PT	PRR	high	low
"lymphadenitis bacterial"	2.011887	4.584324	0.882942
"rib hypoplasia"	2.379827	4.176100	1.356188
"injection site abscess"	1.873744	2.249901	1.560477
"injection site mass"	1.367855	1.552392	1.205254
"lung carcinoma cell type unspecified stage ii"	1.992163	5.460908	0.726750
"ureteric injury"	1.860812	4.578129	0.756339
"miscarriage of partner"	3.023819	7.548391	1.211315
"fallopian tube abscess"	6.512840	16.960287	2.500965
"injection site necrosis"	1.628630	1.968454	1.347471
"follicular thyroid cancer"	1.387982	3.394483	0.567537
"autoimmune pancytopenia"	1.681329	3.591384	0.787125
"progressive multiple sclerosis"	1.253245	1.699026	0.924426
"multiple sclerosis relapse"	2.212657	2.304053	2.124886
"intrathecal pump insertion"	3.150397	8.776513	1.130859
"injection site dryness"	2.052531	4.679119	0.900359
"primary progressive multiple sclerosis"	1.736757	4.743694	0.635860
"dermo-hypodermitis"	1.151931	2.808753	0.472432
"schizoaffective disorder bipolar type"	1.148026	2.589948	0.508877
"injection site erythema"	1.197440	1.250309	1.146807
"injection site fibrosis"	1.672433	3.401182	0.822371
"pelvic sepsis"	2.861981	6.584928	1.243891
"lipoedema"	1.003460	2.712440	0.371227
"carcinoid tumour of the duodenum"	2.454114	6.082939	0.990093
"multiple sclerosis"	1.190841	1.260893	1.124681
"injection site exfoliation"	2.551606	4.709182	1.382553
"intestinal ulcer perforation"	1.151931	2.808753	0.472432
"band sensation"	2.428183	3.867471	1.524530
"bartonellosis"	7.525949	22.237304	2.547067
"intestinal cyst"	1.218229	2.972915	0.499201
"injection site discolouration"	1.051474	1.231720	0.897604
"multiple pregnancy"	1.286080	2.502261	0.661003
"optic neuritis"	2.058452	2.236357	1.894700
"anal cancer metastatic"	2.084109	5.720336	0.759310
"injection site cellulitis"	1.967364	2.380016	1.626259
"neuromyelitis optica"	2.010982	2.725610	1.483723
"injection site infection"	2.294656	2.652482	1.985101
"injection site scab"	3.165807	4.327138	2.316157
"alpha-1 anti-trypsin deficiency"	1.638715	3.718005	0.722265
"fear of needles"	1.744211	3.115726	0.976425
"injection site nerve damage"	1.159821	2.828275	0.475620
"injection site erosion"	2.065047	5.093678	0.837198
"injection site eczema"	4.671279	13.287444	1.642215
"chronic leukaemia"	1.676573	4.115272	0.683040
"secondary progressive multiple sclerosis"	1.405790	1.991225	0.992478
"injection site injury"	1.283372	2.054642	0.801621
"neutralising antibodies"	3.184055	5.907634	1.716120
"congenital mitochondrial cytopathy"	2.463038	6.796372	0.892617
"injection site scar"	1.486316	2.024338	1.091288
"thromboangiitis obliterans"	1.039769	2.205759	0.490135
