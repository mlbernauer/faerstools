PT	PRR	high	low
"suture removal"	1.104536	2.339057	0.521578
"cardiovascular disorder"	1.036466	1.100549	0.976114
"organ failure"	1.336179	1.488661	1.199315
"stevens-johnson syndrome"	1.989619	2.078803	1.904261
"lip neoplasm benign"	2.982248	6.416986	1.385978
"median nerve injury"	1.065088	2.872578	0.394911
"skin injury"	1.021997	1.302094	0.802152
"pericoronitis"	1.014370	2.462445	0.417856
"bladder calculus removal"	1.032813	2.784509	0.383085
"cerebral arteriovenous malformation haemorrhagic"	1.020444	2.750775	0.378550
"pityriasis rubra pilaris"	1.200100	3.241672	0.444289
"hypocalciuria"	2.184797	5.967421	0.799900
"epiploic appendagitis"	1.420118	3.461641	0.582595
