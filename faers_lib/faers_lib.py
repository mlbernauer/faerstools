#!/usr/bin/pyhton
import MySQLdb
import re
import string
import cPickle
import operator

# Return database cursor to use in furture functions
def get_db_connection(password, user="root", db="FAERS"):
	con = MySQLdb.connect(host="localhost", user=user, passwd=password, db=db)
	cursor = con.cursor()
	return cursor

# Return list of distinct drugs in drugs table
def get_drug_list(con, distinct=False):
	con.execute("SELECT drugname FROM drug")
	if distinct:
		return set(con.fetchall())
	return con.fetchall()

def map(drug_list):
	new_drug_list = []
	drug_map = {}
	distinct_drugs = list(set(drug_list))
	for drug in distinct_drugs:
		drug_map.setdefault(drug, distinct_drugs.index(drug))

	for drug in drug_list:
		new_drug_list.append(drug_map[drug])
	return new_drug_list

def new_function():
	pass		
