#!/usr/bin/python
import MySQLdb
import nltk
import cPickle
import shelve
import os
import sys

class session:
	def __init__(self, user="root", passwd="ypO4okyz!", host="localhost", db="FAERS", data=[], import_data=False):
		self.password = passwd
		self.user = user
		con = MySQLdb.connect(host=host, user=user, passwd=passwd, db=db)
		self.cursor = con.cursor()
		self.data = data
		# Import MySQL drug_reactions data if True
		if import_data:
			try:
				self.data = cPickle.load(open("faers_data.pkl", 'rb'))
			except:
				self.cursor.execute("SELECT * FROM drug_reactions")
				self.data = self.cursor.fetchall()
				cPickle.dump(self.data, open("faers_data.pkl", "wb"))
		# If faers_data.db shelve file not created, create it
		if os.path.exists("faers_data.db"):
			self.faers_data = shelve.open("faers_data.db")
		else:
			self.faers_data = shelve.open("faers_data.db")
			d = {"drug_reactions":{}, "drug_counts":{}, "reaction_counts":{}}
			for isr, drug, reaction in self.data:
				d["drug_reactions"].setdefault(drug, {})
				d["drug_reactions"][drug].setdefault(reaction,0)
				d["drug_reactions"][drug][reaction] += 1
				d["drug_counts"].setdefault(drug, 0)
				d["reaction_counts"].setdefault(reaction, 0)
				d["drug_counts"][drug] += 1
				d["reaction_counts"][reaction] += 1
			self.faers_data.update(d)

	def get_atc_dictionary(self, level=1):
		atc_level = "level" + str(level)
		con = MySQLdb.connect(host="localhost", user=self.user, passwd=self.password, db="ChEMBL")
		cursor = con.cursor()
		cursor.execute("SELECT %s, who_name FROM  atc_classification" % atc_level)
		data = cursor.fetchall()
		atc_dict = {}
		for lev, drug in data:
			atc_dict.setdefault(lev, set())
			atc_dict[lev].add(drug)
		return atc_dict

	def get_data(self):
		self.cursor.execute("SELECT * FROM drug_reactions")
		self.data = self.cursor.fetchall()

	def get_drug_codes(self):
		con = MySQLdb.connect(host="localhost", user=self.user, passwd=self.password, db="ChEMBL")
		cursor = con.cursor()
		cursor.execute("SELECT synonyms, level1, level2, level3, level4, level5 FROM atc_classification INNER JOIN molecule_synonyms USING (molregno)")
		self.drug_codes = cursor.fetchall()
		self.atc_dict = {}
		for drug, l1, l2, l3, l4, l5 in self.drug_codes:
			drug = drug.lower()
			self.atc_dict.setdefault(drug, {"level1":"", "level2":"", "level3":"", "level4":"", "level5":""})
			self.atc_dict[drug]["level1"] = l1
			self.atc_dict[drug]["level2"] = l2
			self.atc_dict[drug]["level3"] = l3
			self.atc_dict[drug]["level4"] = l4
			self.atc_dict[drug]["level5"] = l5
	
	def recode_drugs(self, level):
		self.coded = []
		lvl = "level" + str(level)
		for isr, drug, reaction in self.data:
			d = self.atc_dict.get(drug.lower(), 0)
			if d != 0:
				self.coded.append((isr, self.atc_dict[drug.lower()][lvl], reaction))
		
			
			
	

