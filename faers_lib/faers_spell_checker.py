#!/usr/bin/python
import MySQLdb
import string
import difflib

# Submit a query to a database
def getquery(query, db="FAERS"):
	con = MySQLdb.connect(host="localhost", user="root", passwd="ypO4okyz!", db=db)
	cursor = con.cursor()
	cursor.execute(query)
	return cursor.fetchall()

def getdrugnames(country=False):
	if country:
		drugs = getquery("SELECT drugname FROM drugs INNER JOIN demographics USING (isr) WHERE reporter_country = '%s'" % country)
	        return set([i[0].strip().lower() for i in drugs if i[0] is not None])

	drugs = getquery("SELECT drugname FROM drugs")
	return set([i[0].strip().lower() for i in drugs if i[0] is not None])

# Get prefered_names of drugs using ChEMBL database
def getprefnames():
	pref_names = getquery("SELECT DISTINCT(pref_name) FROM molecule_dictionary", db="ChEMBL")
	return set([i[0].lower() for i in pref_names if i[0] is not None])

# Get synonym dictionary
def getsynonyms():
	data = getquery("SELECT synonyms, pref_name FROM molecule_synonyms INNER JOIN molecule_dictionary USING (molregno)", db="ChEMBL")
	synonyms = {}
	for d in data:
		if None not in d:
			synonyms[d[0].strip().lower()] = d[1].strip().lower()
	return synonyms


# Return tuple of (original, corrected) drugname
def correct(drugnames, prefnames, synonyms):
	# Save drugs names thar are already preferred names
	preferred = list(drugnames.intersection(prefnames))
	# Get remaining drugs that are not preferred names
	diff = drugnames.difference(prefnames)
	# Find drugnames from diff that are synonyms 
	syns = diff.intersection(synonyms.keys())
	# Map the syns to their pref. name
	for s in syns: preferred.append(synonyms[s])
	# Get all drug names that are neither syns or pref
	diff = diff.difference(syns)
	# Correct these to either their correct syn or pref
	allnames = list(set(synonyms.keys() + list(prefnames)))
	corrected = []
	for name in diff:
		if name in allnames:
			continue
		matches = difflib.get_close_matches(name, allnames)
		if matches:
			corrected.append((name, matches[0]))
	return preferred, corrected
	
		
