#!/usr/bin/python
import MySQLdb
import re
import string
import cPickle
import operator

# Import drug data from pickle file
def importdata(filename="faers_data.pkl"):
        return cPickle.load(open(filename, 'rb'))

# Return drugreaction data from MySQL database
def getrawdata():
	con = MySQLdb.connect(host='localhost', user='root', passwd='ypO4okyz!', db='FAERS')
	cursor = con.cursor()
	cursor.execute("SELECT * FROM drug_reactions")	
	return cursor.fetchall()

def getdrugdata(drug):
	con = MySQLdb.connect(host='localhost', user='root', passwd='ypO4okyz!', db='FAERS')
	cursor = con.cursor()
	cursor.execute("SELECT * FROM drug_reactions WHERE drugname = '%s'" % drug)
	return cursor.fetchall()

def execute(query):
	con = MySQLdb.connect(host='localhost', user='root', passwd='ypO4okyz!', db='FAERS')
        cursor = con.cursor()
	cursor.execute(query)
	return cursor.fetchall()

def getdrugcounts():
	con = MySQLdb.connect(host='localhost', user='root', passwd='ypO4okyz!', db='FAERS')
        cursor = con.cursor()
	cursor.execute("SELECT drugname, COUNT(drugname) AS dcount FROM drugs GROUP BY drugname ORDER BY dcount DESC")
	data = cursor.fetchall()
	
	data = [(i[0].strip().lower(), int(i[1])) for i in data if i[0] is not None]
	dataDictionary = dict(data)
	sortedData = sorted(dataDictionary.iteritems(), key=operator.itemgetter(1), reverse=True)
	return sortedData


# Get drug names
"""
def getdrugnames(lower=False, clean=True):
	con = MySQLdb.connect(host='localhost', user='root', passwd='ypO4okyz!', db='FAERS')
        cursor = con.cursor()
	cursor.execute("SELECT DISTINCT(drugname) FROM drugs")
	drugnames = cursor.fetchall()
	if clean:
#		regex = 
	if lower:
		return [i[0].lower() for i in drugnames if i[0] is not None]
	return drugnames
"""

# Get reactions 
def getreactions(lower=False):
        con = MySQLdb.connect(host='localhost', user='root', passwd='ypO4okyz!', db='FAERS')
        cursor = con.cursor()
        cursor.execute("SELECT DISTINCT(pt) FROM reactions")
	reactions = cursor.fetchall()
	if lower:
		return [i[0] for i in reactions if i[0] is not None]
	return reactions

# Map drug names to whatever is specified by mappingDictionary
def mapdrugs(mapdict, data):
	mapped_data = []
	rejects = []
	for isr, drug, pt in data:
		try:
			mapped_data.append((isr, mapdict[drug.lower()], pt.lower()))
		except:
			# Store index of unmappable drug name
			rejects.append(data.index((isr, drug, pt)))
	return standardizeddata, rejects

# Create drugreaction dict from drugreaction data
def getdrugreactiondict(data):
	drdict = {"drugreactions":{}, "drugcounts":{}, "reactioncounts":{}}
	for isr, drug, reaciton in data:
		drdict["drugreactions"].setdefault(drug, {})
		drdict["drugreactions"][drug].setdefault(reaction, 0)
		drdict["drugreactions"][drug][reaction] += 1
		drdict["drugcounts"].setdefault(drug,0)
		drdict["drugcounts"][drug] += 1
		drdict["reactioncounts"].setdefault(reaction,0)
		drdict["reactioncounts"][reaction] += 1
	return drdict

def cleanrejects(syndict, rejects):
	regex = re.compile('[%s]' % re.escape(string.punctuation))
	fixed = []
	for drug in rejects:
		found = 0
		d = regex.sub("",drug)
		for word in d.lower().split(" "):
			try:
				syndict[word]
				found += 1
			except:
				pass
		fixed.append((drug, found))
	return fixed

# Pull atcclassification data from ChEMBL
def getatccodes():
	con = MySQLdb.connect(host="localhost", user="root", passwd="ypO4okyz!", db="ChEMBL")
	cursor = con.cursor()
	cursor.execute("SELECT synonyms, level1, level2, level3, level4, level5 FROM atc_classification INNER JOIN molecule_synonyms USING (molregno)")
	return cursor.fetchall()

# Create atcdict using ChEMBL atc data
def getatcdict(atccodes):
	atcdict = {}
	for syn, l1, l2, l3, l4, l5 in atccodes:
		s = syn.lower()
		atcdict.setdefault(s, {1:"", 2:"", 3:"", 4:"", 5:""})
		atcdict[s][1] = l1
		atcdict[s][2] = l2
		atcdict[s][3] = l3
		atcdict[s][4] = l4
		atcdict[s][5] = l5
	return atcdict

# Return drug synonym dictionary of type syn:prefname
def getsyndict():
	con = MySQLdb.connect(host="localhost", user="root", passwd="ypO4okyz!", db="ChEMBL")
	cursor = con.cursor()
	cursor.execute("SELECT synonyms, pref_name FROM molecule_synonyms INNER JOIN molecule_dictionary USING (molregno)")
	data = cursor.fetchall()
	syndict = {}
	for syn, pref in data:
		try: 
			syndict[syn.lower()] = pref.lower()
		except:
			pass
	return syndict
