#!/usr/bin/python
import spellChecker
import re
import sys

dictFile = sys.argv[2]
misspells = sys.argv[1]

spellChecker.NWORDS = spellChecker.getWords(dictFile)

f = open(misspells, "r")
f = f.read()
f = f.lower()
f = f.split("\n")
for word in f:
	w = re.findall("[a-z]+", word.lower().rstrip())
	results = [spellChecker.correct(r) for r in w]
	if w != results:
		print list(set(w).difference(set(results))), "\t", list(set(results).difference(set(w)))

