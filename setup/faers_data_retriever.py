#!/usr/bin/python
# Run this script to retrieve data from the FDA
# FAERS website. This will download all into a folder
# called "faers_data" located in the current directory.
#
# Running the script: ./faers_data_retriever.py
#
#


from BeautifulSoup import *
import urllib2
from subprocess import call

TARGET_DIRECTORY="data"
BASE_URL="http://www.fda.gov"
TEMP_URLS=TARGET_DIRECTORY+"/faers_data_urls.txt"
PARENT_PAGE="http://www.fda.gov/Drugs/GuidanceComplianceRegulatoryInformation/Surveillance/AdverseDrugEffects/ucm083765.htm"

# Scan the parent page for links to FDA AERS data and save the links
def get_data_links(page):
    data_links = set()
    print "Retrieving FAERS data links..."
    try:
        c = urllib2.urlopen(page)    
    except:
        print "Could not open page: %s" % page
    soup = BeautifulSoup(c.read())
    links = soup('a')
    for link in links:
        d = dict(link.attrs)
        if 'href' in d and ".zip" == d['href'][-4:]:
            data_links.add("%s%s" % (BASE_URL, d['href']))
    return list(data_links)

# Create a URL list as text file
def create_url_file(links):
    call(['mkdir', TARGET_DIRECTORY])
    print "Saving FAERS data URLs to %s" % TEMP_URLS
    f = open(TEMP_URLS, 'wb')
    for url in links:
        f.write(url+"\n")
    f.close()

# Download the data from the FDA site
def download_data(data_links):
    for url in data_links:
        call(['wget', '-c','-P',TARGET_DIRECTORY, url])

# Main
if __name__ == '__main__':
  l = get_data_links(PARENT_PAGE)
  create_url_file(l)
  download_data(l)
