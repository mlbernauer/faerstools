#!/bin/bash
#######################################
# Michael Bernauer
# Program used to extract and load FAERS data
# into MySQL table
# The faers_schema.sql file must be in the same
# directory as this script when executed. 

# Example:
# ./load_db.sh <user> <passwd> ../FAERS_DATA/ASCII_DATA/test_data

#######################################

# User and password for MySQL database
USER=$1
PASSWD=$2

# The data directory
SOURCE=$3

mysql -u$USER -p$PASSWD -e "DROP TABLE IF EXISTS"
mysql -u$USER -p$PASSWD < "./faers_schema.sql"

# Create database
for file in `ls $SOURCE`
	do
	# Determine which file we are loading into DB
	table=`echo $file | tr "[:upper:]" "[:lower:]" | sed -s "s/[0-9].*//g"`
	if [ "$table" != "stat" ]
	then
		echo "Loading $table into faers..."
		mysql -u$USER -p$PASSWD faers --enable-local-infile -e "LOAD DATA LOCAL INFILE '$SOURCE/$file' INTO TABLE $table FIELDS TERMINATED BY '$' LINES TERMINATED BY '\n' IGNORE 1 LINES";
	fi
done
 
