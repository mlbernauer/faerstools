#!/bin/bash
# This script will extract and all the data
# that was downloaded from the FDA AERS site. 
# The script requires user to pass the source 
# direcoty of the data to sort as well as the 
# target directory which will be created 
#
# Usage: ./faers_extractor.sh ./source_folder ./target_folder
#
#
SOURCE_DIRECTORY=$1
TARGET_DIRECTORY=$2

mkdir $TARGET_DIRECTORY
cd $TARGET_DIRECTORY
mkdir ASCII_DATA
mkdir SGML_DATA
mkdir DOCS
cd ../$SOURCE_DIRECTORY
cd $SOURCE_DIRECTORY

for file in `ls *.zip`;
do
    cd $SOURCE_DIRECTORY
    DIR=`echo $file | sed -e "s/\.zip//"`
    mkdir $TARGET_DIRECTORY/$DIR
    cp $file $TARGET_DIRECTORY/$DIR
    cd $TARGET_DIRECTORY/$DIR
    unzip $file
done

find -name '*.TXT' -exec mv -f {} ../$TARGET_DIRECTORY/"ASCII_DATA" \;
find -name "*.SGM" -exec mv -f {} ../$TARGET_DIRECTORY/"SGML_DATA" \;
find -name "*.doc" -exec mv -f {} ../$TARGET_DIRECTORY/"DOCS" \;
find -name "SIZE*TXT" -exec mv -f {} ../$TARGET_DIRECTORY/"DOCS" \;
rm -rf U*
rm -rf u*
