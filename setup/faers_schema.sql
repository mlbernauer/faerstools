DROP DATABASE IF EXISTS faers;
CREATE DATABASE faers;
USE faers;
CREATE TABLE drug(
	isr INT(10),
	drug_seq INT(10),
	role_code CHAR(10),
	drugname CHAR(255),
	val_vbm INT(10),
	route CHAR(255),
	dose_vbm CHAR(255),
	dechal CHAR(10),
	rechal CHAR(10),
	lot_num CHAR(255),
	exp_dt INT(10),
	nda_num CHAR(10));

CREATE TABLE demo(
	isr INT(10),
	case_num INT(10),
	if_code	CHAR(10),
	foll_seq CHAR(10),
	image	CHAR(255),
	event_dt INT(10),
	mfr_dt INT(10),
	fda_dt INT(10),
	rept_code CHAR(10),
	mfr_num CHAR(255),
	mfr_sndr CHAR(255),
	age FLOAT(7,2),
	age_code CHAR(10),
	gndr_code CHAR(10),
	e_sub CHAR(10),
	wt FLOAT(11,5),
	wt_code CHAR(3),
	rept_dt INT(10),
	occp_code CHAR(10),
	death_dt INT(10),
	to_mfr CHAR(10),
	onfid CHAR(10),
	reporter_country CHAR(255));

CREATE TABLE indi(
	isr INT(10),
	drug_seq INT(10),
	indi_pt	CHAR(255));

CREATE TABLE outc(
	isr INT(10),
	out_cod CHAR(10));

CREATE TABLE reac(
	isr INT(10),
	pt CHAR(255));

CREATE TABLE rpsr (
	isr INT(10),
	rpsr_code CHAR(10));

CREATE TABLE ther(
	isr INT(10),
	drug_seq INT(10),
	start_dt INT(10),
	end_dt INT(8),
	dur INT(5),
	dur_cod CHAR(10));
