#!/usr/bin/python
import sys
import Levenshtein
import re

splitter = re.compile("\\W+")

def scan_for_closest_match(drug, drugmap):
  druglist = drugmap.keys()
  dists = [Levenshtein.distance(i, drug) for i in druglist]
  sorted_idx = [i[0] for i in sorted(enumerate(dists), key=lambda x:x[1])]
  closest_match = druglist[sorted_idx[0]]
  return drugmap[closest_match]

# Create drugmap dictionary
def get_drugmap(filename):
  f = open(filename,"r")
  drugmap = {}
  for line in f:
    cols = line.strip().split("\t")
    syn = cols[1].upper()
    name = cols[0].upper()
    drugmap.setdefault(name,name)
    drugmap.setdefault(syn, name)
  return drugmap

# get unmapped lines
def unmapped(filename):
  # Faers misspellings
  return open(filename, "r").readlines()

def map_drug(drug, drugmap):
  drug = drug.upper()
  if drug in drugmap:
    return "E:"+drugmap[drug]
  else:
    candidates = splitter.split(drug)
    #candidates = drug.split(" ")
    #predictions = list(set([scan_for_closest_match(i, drugmap) for i in candidates]))
    matches = list(set([i for i in candidates if i in drugmap]))
    try:
      return "P:"+matches[0]
    except:
      return "N:"+drug

def map_druglist(unmapped, drugmap):
  match_counts = {}
  for drug in unmapped:
    drug = drug.strip()
    mapped = map_drug(drug, drugmap)
    match_counts.setdefault(mapped[0],0)
    match_counts[mapped[0]] += 1
  print match_counts

if __name__ == "__main__":
  
  dmap = get_drugmap(sys.argv[1])
  unmapped = unmapped(sys.argv[2])
  
  map_druglist(unmapped, dmap)
