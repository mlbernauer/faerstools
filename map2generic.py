#!/usr/bin/python
import sys

def map2gen(filename, mapfile):
  maps = {}
  f = open(mapfile, "r")
  for line in f:
    cols = line.strip().split("\t")
    syn = cols[0].lower()
    gen = cols[1].lower()
    maps.setdefault(syn, gen)
  
  f = open(filename, "r")
  for line in f:
    drug = line.strip().lower()
    try:
      print maps[drug]
    except:
      print "na"

if __name__ == "__main__":
  map2gen(sys.argv[1], sys.argv[2])
