#!/bin/bash
mysql -uroot -p$1 chembl -e "select molecule_dictionary.pref_name, synonyms from molecule_dictionary inner join molecule_synonyms using (molregno)" > $2
cat $2 | sort | uniq | > $2"_uniq.txt"
