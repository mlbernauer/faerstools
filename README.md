#######################################################
# Description
#######################################################
This is a collection of tools for setting up and
exploring the FDA Adverse Event Reporting System
database. Dependencies include Python and MySQL. 

Michael Bernauer
Feb 22, 2014

SETTING UP THE DATABASE

1) Download the FAERS data from the FDA site.
    ./faers_data_retriever.py

2) Extract the data archives
    ./faers_extractor.sh <data_source> <target_dir>

3) 
