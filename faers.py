import math
import subprocess as sp
import time

class Faers:
  def __init__(self, filename):
    self.associations = {}
    self.drugs = {}
    self.pt = {}
    self.total_cases = 0
    self.isr = set()
    f = open(filename, "r")
    for line in f:
      cols = line.strip().split("\t")
      self.associations.setdefault(cols[1].lower(),{})
      self.associations[cols[1].lower()].setdefault(cols[2].lower(), 0)
      self.associations[cols[1].lower()][cols[2].lower()] += 1
      self.drugs.setdefault(cols[1].lower(), 0)
      self.pt.setdefault(cols[2].lower(),0)
      self.drugs[cols[1].lower()] += 1
      self.pt[cols[2].lower()] += 1
      self.total_cases += 1
      self.isr.add(cols[0])
    self.total_cases = len(self.isr)
   
  # Proportioanl Reporting Ratio 
  def prr(self, drug, pt):
    A = self.associations[drug][pt]
    A_C = self.pt[pt]
    A_B = self.drugs[drug]
    A_B_C_D = self.total_cases
    C = A_C - A
    C_D = A_B_C_D - A_B
    prr =  float(A/float(A_B)) / float(C/float(C_D))
    s = math.sqrt(1/float(A) + 1/float(C) - 1/float(A_B) - 1/(C_D))
    prr_lower = float(prr/math.exp(1.96*s))
    prr_upper = float(prr*math.exp(1.96*s))
    return {"PRR":prr, "PRR-":prr_lower, "PRR+":prr_upper, "A":A, "AB":A_B, "C":C, "CD":C_D}

  def prr_report(self, drug, A=0, PRR=0):
    for pt in self.associations[drug]:
      try:
        if self.prr(drug,pt)["A"] > A and self.prr(drug,pt)["PRR"] > PRR:
          print pt, self.prr(drug, pt)
      except:
        pass

  def save_prr_report(self, drug, A=0, PRR=0, outfile="tmp_prr_data.txt"):
    f = open(outfile,"w")
    f.write("PT\tPRR\thigh\tlow\n")

    for pt in self.associations[drug]:
      try:
        if self.prr(drug,pt)["A"] > A and self.prr(drug,pt)["PRR"] > PRR:
          r = self.prr(drug,pt)
          f.write("\"%s\"\t%f\t%f\t%f\n" % (pt, r["PRR"], r["PRR+"], r["PRR-"]))

      except:
        pass
    f.close()
    print "Data written to %s" % outfile
    time.sleep(2)
    sp.call(["forestplot.R", outfile[:-4], outfile])

  # Chi squared
  def chi_squared(self, drug, pt):
    A = self.associations[drug][pt]
    A_C = self.pt[pt]
    A_B = self.drugs[drug]
    A_B_C_D = self.total_cases
    C = A_C - A
    C_D = A_B_C_D - A_B
    D = C_D - C
    B = A_B - A
    return ((A*D - B*C)**2)*(A_B_C_D)/float( (A_B * C_D * A_C * B+D)) 

  # Odds ratio
  def odds(self, drug1, drug2, pt):
    drug1_pt = self.associations[drug1][pt]
    drug2_pt = self.associations[drug2][pt]
    drug1 = self.drugs[drug1]
    drug2 = self.drugs[drug2]
    odds = float(drug1_pt / float(drug1)) / float(drug2_pt / float(drug2))
    return (odds, drug1_pt, drug1, drug2_pt, drug2)
